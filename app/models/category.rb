class Category < ActiveRecord::Base
  has_many :items
  
  default_scope -> { order('name') }
  validates :name, presence: true, uniqueness: true,
                   length: { maximum: 80 }
  validates :description, presence: true, length: { maximum: 255 }
end
