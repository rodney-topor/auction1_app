class Item < ActiveRecord::Base
  belongs_to :category
  
  default_scope -> { order('created_at DESC') }
  validates :name, presence: true, uniqueness: true
  validates :description, presence: true, length: { maximum: 200 }
  validates :vendor, presence: true
  validates :starting_price, presence: true, 
                             numericality: { greater_than: 0 }
  
  def Item.search(category_id, query)
    # naive database-based implementation 
    if category_id.blank? && query.blank?
      Item.all
    elsif category_id.blank?
      Item.where('name like ? or description like ?', "%#{query}%", "%#{query}%")
    elsif query.blank?
      Item.where('category_id == ?', category_id)
    else
       Item.where('category_id == ? and (name like ? or description like ?)', 
                  category_id, "%#{query}%", "%#{query}%")
    end
  end
end
