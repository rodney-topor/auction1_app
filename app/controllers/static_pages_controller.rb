class StaticPagesController < ApplicationController
  def about
  end
  
  def contact
  end

  def help
  end
  
  def documentation
  end
end
