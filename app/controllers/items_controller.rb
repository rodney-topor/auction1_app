class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]
  before_action :set_category_options, only: [:new, :create, :index, :edit, :update]
  
  def new
    @item = Item.new
  end
  
  def create
    @item = Item.new(item_params)
    if @item.save
      flash[:success] = "New item added."
      redirect_to @item
    else
      render 'new'
    end
  end
  
  def index
    @items = Item.search(params[:category_id], params[:query]).paginate(page: params[:page], per_page: 4)
  end
  
  def show
    @category = Category.find(@item.category_id)
  end
  
  def edit
  end
  
  def update
    if @item.update_attributes(item_params)
      flash[:success] = "Item details updated."
      redirect_to @item
    else
      render 'edit'
    end
  end
  
  def destroy
    @item.destroy
    flash[:success] = "Item deleted."
    redirect_to items_url
  end
  
  private
  
    def item_params
      params.require(:item)
            .permit(:name, :description, :category_id, :vendor, :starting_price)
    end
  
    def set_item
      @item = Item.find(params[:id])
    end
  
    def set_category_options
      @category_options = Category.all.collect { |c| [c.name, c.id] }
      # @category_options = []
    end
end
