This is a sample solution to Assignment 1 in the summer 2014 offering
of 2503ICT Web Programming at Griffith University.

It implements the first part of a simple online auction system.

Users may browse items for sale by category 
or search for them by name or description.  They may offer new
items for sale.  They may edit and delete existing items.

The implementation contains a few convenience features that go
beyond the specification.  These include pagination, extra links,
flash, and a debug box.

On the other hand, the user interface is too simple, links aren't
obviously links, and headings for item lists are crude.

To install, clone from https://bitbucket.org/rodney-topor/auction_app
and perform the following git commands:

bundle install
rake db:reset
rake db:populate

The root path is /.  

Rodney Topor, January 2014