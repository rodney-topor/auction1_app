namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    # Explicit category definitions
    books  = Category.create!(name: "Books", description: "readable items")
    movies = Category.create!(name: "Movies", description: "watchable items")
    music  = Category.create!(name: "Music", description: "listenable items")
    
    # Explicit item definitions
    Item.create!(name: "Catch 22", vendor: "Rodney", category_id: books.id,
                 description: "Great anti-war novel by Joseph Heller.",
                 starting_price: 9.99)
    Item.create!(name: "Pride and Prejudice", vendor: "Rodney", category_id: books.id,
                 description: "Great 10th century romantic novel by Jane Austen.",
                 starting_price: 9.99)
    Item.create!(name: "War and Peace", vendor: "Rodney", category_id: books.id,
                 description: "Great historical novel by Leo Tolstoy.",
                 starting_price: 9.99)
    Item.create!(name: "Midnight's Children", vendor: "Rodney", category_id: books.id,
                 description: "Great Indian novel by Salman Rushdie.",
                 starting_price: 9.99)
    Item.create!(name: "Manhattan", vendor: "Rodney", category_id: movies.id,
                 description: "Great romantic movie by Woody Allen.",
                 starting_price: 9.99)
    Item.create!(name: "Star Wars", vendor: "Rodney", category_id: movies.id,
                 description: "Great sci-fi movie by George Lucas and Steven Spielberg.",
                 starting_price: 9.99)
    Item.create!(name: "Sergeant Pepper's Lonely Hearts Club Band", vendor: "Rodney", category_id: music.id,
                 description: "The Beatles' greatest album, now available for free on YouTube.",
                 starting_price: 9.99)
  end
end